/**
 * @file	white_box_tests.cpp
 * @author	CYRIL URBAN
 * @date:	2017-01-04
 * @brief	White Box - Tests suite
 */

#include "gtest/gtest.h"
#include "white_box_code.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy operaci nad maticemi. Cilem testovani je:
// 1. Dosahnout maximalniho pokryti kodu (white_box_code.cpp) testy.
// 2. Overit spravne chovani operaci nad maticemi v zavislosti na rozmerech 
//    matic.
//============================================================================//

// The fixture for testing class Matrix
class WhiteBoxTest : public ::testing::Test {
 protected:
};

/**
 * @brief      TEST_F
 *
 * @param      WhiteBoxTest
 * @param      BasicMatrix
 *
 */
TEST_F(WhiteBoxTest, BasicMatrix) {

	// create new Matrix A
	Matrix MatrixA;
	double value = 5.0;

	// set value on position [0,0]
	bool set = MatrixA.set(0,0,value);
	ASSERT_TRUE(set);

	// set value out of matrix position
	set = MatrixA.set(1,0,value);
	ASSERT_FALSE(set);

	// get value from position [0,0] 
	double get = MatrixA.get(0,0);
	ASSERT_EQ(value, get);

	// get value out of matrix position
	get = MatrixA.get(0,1);
	ASSERT_NE(value, get);
}

/**
 * @brief      TEST_F
 *
 * @param      WhiteBoxTest
 * @param      SetMatrix2x3
 *
 */
TEST_F(WhiteBoxTest, SetMatrix2x3) {

	// create new Matrix A with dimension 2x3
	Matrix MatrixA(2,3);

	// set values matrix 2x3
	std::vector<std::vector< double > > Values {{22,1,16},{112,0,4}};
	bool set = MatrixA.set(Values);
	ASSERT_TRUE(set);

	// here is finded error in code white_box_code:49
}

/**
 * @brief      TEST_F
 *
 * @param      WhiteBoxTest
 * @param      MultidimensionalMatrix
 *
 */
TEST_F(WhiteBoxTest, MultidimensionalMatrix) {

	// bad dimensional matrix
	Matrix *BadMatrix;
 	ASSERT_ANY_THROW(BadMatrix = new Matrix(0,-1));

 	// create new Matrix A with dimension 3x2
	Matrix MatrixA(3,2);

	// wrong set values (values for matrix 2x3)
	std::vector<std::vector< double > > BadValues { {1,2,3}, {4,5,6} };
	bool set = MatrixA.set(BadValues);
	ASSERT_FALSE(set);

	// Matrix A: set correct values
	std::vector<std::vector< double > > values { {1,2}, {3,4}, {5,6} };
	// set matrix values from array  
	set = MatrixA.set(values);
	ASSERT_TRUE(set);
	/*
	matrix looks:
	  | 0:1:
	---------
	0:|(1,2)
	1:|(3,4)
	2:|(5,6)
	*/
	// get value from position [2,1] 
	double get = MatrixA.get(2,1);
	ASSERT_EQ(6, get);

	//operator==
	//	
	bool equals;
	// wrong matrix size (4x2)
	Matrix MatrixWrong(4,2);
	std::vector<std::vector< double > > BadValues2 { {1,2}, {3,4}, {5,6}, {7,8}};
	set = MatrixWrong.set(BadValues2);
	ASSERT_ANY_THROW(equals = MatrixA.operator==(MatrixWrong));

	// create new Matrix B with dimension 3x2
	Matrix MatrixB(3,2);
	std::vector<std::vector< double > > ValuesB { {11,12}, {13,14}, {15,16} };
	set = MatrixB.set(ValuesB);
	ASSERT_TRUE(set);

	// not equal
	equals = MatrixA.operator==(MatrixB);
	ASSERT_FALSE(equals);
	
	// equal
	ValuesB = { {1,2}, {3,4}, {5,6} };
	set = MatrixB.set(ValuesB);
	equals = MatrixA.operator==(MatrixB);
	ASSERT_TRUE(equals);

	// operator+
	//	
	// wrong sum two matrix (difficul dimension)
	Matrix SumMatrix(3,2);
	ASSERT_ANY_THROW(SumMatrix = MatrixWrong.operator+(MatrixB));
	
	// correct sum two matrix
	SumMatrix = MatrixA.operator+(MatrixB);
	get = SumMatrix.get(0,0);
	ASSERT_EQ(2, get);	
}

/**
 * @brief      TEST_F
 *
 * @param      WhiteBoxTest
 * @param      MatrixMultiplication
 *
 * operator* 
 */
TEST_F(WhiteBoxTest, MatrixMultiplication) {

	// create new Matrix A with dimension 2x2
	Matrix MatrixA(2,2);
	std::vector<std::vector< double > > ValuesA {{1,2},{3,4}};
	bool set = MatrixA.set(ValuesA);
	ASSERT_TRUE(set);

	// Matrix multiplication 2x2
	Matrix MulMatrix(2,2);

	// wrong multiplication two matrix (difficul dimension)
	Matrix MatrixWrong(3,3);
	ASSERT_ANY_THROW(MulMatrix = MatrixWrong.operator*(MatrixA));

	// create new Matrix B with dimension 2x2
	Matrix MatrixB(2,2);
	std::vector<std::vector< double > > ValuesB {{5,6},{7,8}};
	set = MatrixB.set(ValuesB);
	ASSERT_TRUE(set);

	// correct multiplication two matrix
	MulMatrix = MatrixA.operator*(MatrixB);
	double get = MulMatrix.get(0,0);
	ASSERT_EQ(19, get);	

	// multiplication matrix with scalar
	MulMatrix = MatrixA.operator*(5);
	get = MulMatrix.get(0,0);
	ASSERT_EQ(5, get);
	get = MulMatrix.get(0,1);
	ASSERT_EQ(10, get);
}

/**
 * @brief      TEST_F
 *
 * @param      WhiteBoxTest
 * @param      SolveEquation
 *
 * Solve equation
 */
TEST_F(WhiteBoxTest, SolveEquation) {

	// create new Matrix A with dimension 2x2
	Matrix MatrixA(2,2);
	std::vector<std::vector< double > > ValuesA {{1,2},{3,4}};
	bool set = MatrixA.set(ValuesA);
	ASSERT_TRUE(set);

	// right side
	std::vector<double> b {{5},{6}};

	// solve equation
	std::vector<double> result;	
	result = MatrixA.solveEquation(b);
	double x1 = result[0];
	double x2 = result[1];
	ASSERT_EQ(-4, x1);
	ASSERT_EQ(4.5, x2);

	// Matrix must be square
	Matrix Matrix3x2(3,2);
	std::vector<std::vector< double > > ValuesB {{1,2},{3,4},{5,6}};
	set = Matrix3x2.set(ValuesB);
	ASSERT_TRUE(set);
	ASSERT_ANY_THROW(result = Matrix3x2.solveEquation(b));

	// The number of elements on the right side 
	// must be equal like number of rows in the matrix
	Matrix Matrix3x3(3,3);
	std::vector<std::vector< double > > ValuesC {{1,2,3},{4,5,6},{7,8,90}};
	set = Matrix3x3.set(ValuesC);
	ASSERT_TRUE(set);
	ASSERT_ANY_THROW(result = Matrix3x3.solveEquation(b));

	// singular matrix
	Matrix SingularMatrix(2,2);
	std::vector<std::vector< double > > ValuesD {{2,2},{2,2}};
	set = SingularMatrix.set(ValuesD);
	ASSERT_TRUE(set);
	ASSERT_ANY_THROW(result = SingularMatrix.solveEquation(b));

	// Solve equation for matrix 3x3
	std::vector<double> b2 {{5},{6},{7}};	
	result = Matrix3x3.solveEquation(b2);

	// Solve equation for matrix 1x1
	Matrix Matrix1x1;
	double value = 5.0;
	set = Matrix1x1.set(0,0,value);
	ASSERT_TRUE(set);
	std::vector<double> b3 {1};	
	result = Matrix1x1.solveEquation(b3);

	// Solve equation for matrix 4x4
	Matrix Matrix4x4(4,4);
	std::vector<std::vector< double > > ValuesE {{10,2,3,4}, {5,6,7,8}, {9,10,151,12}, {13,14,15,16}};
	set = Matrix4x4.set(ValuesE);
	ASSERT_TRUE(set);
	std::vector<double> b4 {{5},{6},{7},{8}};	
	result = Matrix4x4.solveEquation(b4);
}


/**
 * @brief      main
 *
 * @param      argc
 * @param      **argv
 *
 * run all tests
 */
int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

/*** Konec souboru white_box_tests.cpp ***/
