/**
 * @file	black_box_tests.cpp
 * @author	CYRIL URBAN
 * @date:	2017-01-04
 * @brief	Red-Black Tree - public interface tests
 */

#include <vector>

#include "gtest/gtest.h"

#include "red_black_tree.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy Red-Black Tree, testujte nasledujici:
// 1. Verejne rozhrani stromu
//    - InsertNode/DeleteNode a FindNode
//    - Chovani techto metod testuje pro prazdny i neprazdny strom.
// 2. Axiomy (tedy vzdy platne vlastnosti) Red-Black Tree:
//    - Vsechny listove uzly stromu jsou *VZDY* cerne.
//    - Kazdy cerveny uzel muze mit *POUZE* cerne potomky.
//    - Vsechny cesty od kazdeho listoveho uzlu ke koreni stromu obsahuji
//      *STEJNY* pocet cernych uzlu.
//============================================================================//

// The fixture for testing class BinaryTree
class BlackBoxTest : public ::testing::Test {
 protected:

	// create new pointer root
	BinaryTree::Node_t* root;
};


/**
 * @brief      TEST_F
 *
 * @param      BlackBoxTest
 * @param      EmptyTree
 *
 * Checks the basic operation with the empty list.
 * Checks these function: DeleteNode, FindNode, InsertNode
 */
TEST_F(BlackBoxTest, EmptyTree) {

	// create new tree
	BinaryTree tree;

	// find out pointer on root
	root = tree.GetRoot();

	// expectation NULL - no root
	ASSERT_TRUE(root == NULL);


	// DeleteNode
	// delete node in the empty tree
	bool IsDelete = tree.DeleteNode(1);

	// expectation FALSE because tree is empty
	ASSERT_FALSE(IsDelete);


	// FindNode
	// find node in the empty tree
	BinaryTree::Node_t* find;
	find = tree.FindNode(1);

	// expectation NULL - node is not find
	ASSERT_TRUE(find == NULL);


	// InsertNode
	// insert new node in the empty tree
	std::pair<bool, BinaryTree::Node_t*> x = tree.InsertNode(1);

	// expectation not NULL - root is exist
	root = tree.GetRoot();
	ASSERT_TRUE(root != NULL);

	// second way - check the functionality in feature InsertNode
	ASSERT_TRUE(x.first);
	ASSERT_TRUE(x.second != NULL);

	// DeleteNode (clean)
	IsDelete = tree.DeleteNode(1);
}

/**
 * @brief      TEST_F
 *
 * @param      BlackBoxTest
 * @param      NotEmptyTree
 *
 * Checks the basic operation with the not empty list.
 * Checks these function: InsertNode, DeleteNode, FindNode
 */
TEST_F(BlackBoxTest, NotEmptyTree) {

	// create new tree
	BinaryTree tree;

	// InsertNode
	// insert new node in the empty tree
	std::pair<bool, BinaryTree::Node_t*> x = tree.InsertNode(1);

	// expectations not NULL - root is exist
	root = tree.GetRoot();
	ASSERT_TRUE(root != NULL);

	// second way - check the functionality of feature InsertNode
	ASSERT_TRUE(x.first);
	ASSERT_TRUE(x.second != NULL);

	// FindNode
	// find node in the not empty tree
	BinaryTree::Node_t* find;
	find = tree.FindNode(1);

	// expectation not NULL - node is find (and value is pointer on this node)
	ASSERT_TRUE(find != NULL);


	// InsertNode
	// insert new node in the not empty tree (on the same place)
	std::pair<bool, BinaryTree::Node_t*> y = tree.InsertNode(1);

	// check the functionality of feature InsertNode
	ASSERT_FALSE(y.first);
	ASSERT_TRUE(y.second != NULL);
	ASSERT_EQ(x.second, y.second); // compare pointers, they should be the same


	// DeleteNode
	// delete node in the not empty tree
	bool IsDelete = tree.DeleteNode(1);

	// expectation TRUE because node exists
	ASSERT_TRUE(IsDelete);

	
	// FindNode
	// find node in the empty tree
	find = tree.FindNode(1);

	// expectation NULL - node is not find
	ASSERT_TRUE(find == NULL);
}

/**
 * @brief      TEST_F
 *
 * @param      BlackBoxTest
 * @param      axioms
 *
 * Checks axioms in binary tree
 */
TEST_F(BlackBoxTest, axioms) {

	// create new tree
	BinaryTree tree;

	// insert 1000 nodes (and find them)
	std::pair<bool, BinaryTree::Node_t*> x;
	BinaryTree::Node_t* find;
	
	for(int i = 0; i < 1000; i++) {
		// InsertNode
		x = tree.InsertNode(i);
		// check the functionality of feature InsertNode
		ASSERT_TRUE(x.first);
		ASSERT_TRUE(x.second != NULL);

		// FindNode
		find = tree.FindNode(i);
		// expectation not NULL - node is find (and value is pointer on this node)
		ASSERT_TRUE(find != NULL);
	}
	
	
	// 1. axiom
	// each leaf node must have color black (= 1)
	std::vector<BinaryTree::Node_t*> outLeafNodes;
	// function GetLeafNodes makes array of leaf nodes
	tree.GetLeafNodes(outLeafNodes);

	BinaryTree::Node_t* node;
	int color;

	// outLeafNodes.size() = mount of leaf nodes
	for(int i = 0; i < outLeafNodes.size(); i++) {
		node = outLeafNodes[i];
		color = node->color;
		ASSERT_EQ(1, color); // BLACK = 1
	}


	// 2. axiom
	// each red node must have black sons
	std::vector<BinaryTree::Node_t*> outAllNodes;
	// function GetAllNodes makes array of all nodes
	tree.GetAllNodes(outAllNodes);

	// outAllNodes.size() = mount of all nodes
	for(int i = 0; i < outAllNodes.size(); i++) {
		node = outAllNodes[i];
		color = node->color;

		// RED (father) = 0
		if(color == 0) {
			// BLACK (his sons) = 1
			ASSERT_EQ(1, node->pLeft->color); 
			ASSERT_EQ(1, node->pRight->color);
		}
	}


	// 3. axiom
	// All paths from each leaf node to the root of the tree 
	// contains the same number of black node.

	// function GetLeafNodes makes array of leaf nodes
	tree.GetLeafNodes(outLeafNodes);
	// find out root
	root = tree.GetRoot();
	// counts
	int count0 = 0;
	int countI = 0;

	// find out count of black nodes on path from 1. leaf to root
	node = outLeafNodes[0];
	while (node != root) {
		if (node->color == 1) {
			count0++;
		}
		node = node->pParent;
	}
	
	// find out count of black nodes on path from each leafs to root
	// outLeafNodes.size() = mount of leaf nodes
	for(int i = 1; i < outLeafNodes.size(); i++) {
		
		node = outLeafNodes[i];
		
		while (node != root) {
			if (node->color == 1) {
				countI++;
			}
			node = node->pParent;
		}
		// count from 1. leaf must be equal with others counts
		ASSERT_EQ(count0, countI);
		// reset count
		countI = 0;
	}
}

/**
 * @brief      main
 *
 * @param      argc
 * @param      **argv
 *
 * run all tests
 */
int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

/*** Konec souboru black_box_tests.cpp ***/