/**
 * @file	tdd_code.cpp
 * @author	CYRIL URBAN
 * @date:	2017-01-04
 * @brief	Test Driven Development - priority queue code
 */

#include <stdlib.h>
#include <stdio.h>

#include "tdd_code.h"

//============================================================================//
// ** ZDE DOPLNTE IMPLEMENTACI **
//
// Zde doplnte implementaci verejneho rozhrani prioritni fronty (Priority Queue)
// 1. Verejne rozhrani fronty specifikovane v: tdd_code.h (sekce "public:")
//    - Konstruktor (PriorityQueue()), Destruktor (~PriorityQueue())
//    - Metody Insert/Remove/Find a GetHead
//    - Pripadne vase metody definovane v tdd_code.h (sekce "protected:")
//
// Cilem je dosahnout plne funkcni implementace prioritni fronty implementovane
// pomoci tzv. "double-linked list", ktera bude splnovat dodane testy 
// (tdd_tests.cpp).
//============================================================================//

PriorityQueue::PriorityQueue()
{
	root->pNext = NULL;
}

PriorityQueue::~PriorityQueue()
{
	// remove element 
	Element_t *RemoveElement;
	RemoveElement = root->pNext;
	
	while (RemoveElement != NULL) {
		// tmp element 
		Element_t *TmpElement;
		TmpElement = RemoveElement->pNext;
		// delete element
		free(RemoveElement);
		RemoveElement = TmpElement;
	}

	free(RemoveElement);
	//delete root;
	free(root);

	RemoveElement = NULL;
	root = NULL;
}

void PriorityQueue::Insert(int value)
{
	// new element
	Element_t *NewElement = (Element_t*)malloc(sizeof(*NewElement)); //ekvivalent s: new Element_t
	NewElement->value = value;
	// tmp element 
	Element_t *TmpElement;
	TmpElement = root;

	// new element is only element in the queue
	if (TmpElement->pNext == NULL) {
		NewElement->pNext = TmpElement->pNext;
		NewElement->pPrev = NULL;
		TmpElement->pNext = NewElement;
	}
	// new element is not only element in the queue
	else {
		 while(TmpElement->pNext != NULL){

			if (NewElement->value <= TmpElement->pNext->value) {
				NewElement->pNext = TmpElement->pNext;
				TmpElement->pNext = NewElement;

				// root pPrev is NULL
				if (TmpElement == root) {
					NewElement->pPrev = NULL;
				}
				else {
					NewElement->pPrev = TmpElement;
				}

				NewElement->pNext->pPrev = NewElement;
				break;				
			}
			else {
				TmpElement = TmpElement->pNext;	
			}
		} 

		// new element is the last element in the queue
		if (TmpElement->pNext == NULL) {
			NewElement->pNext = TmpElement->pNext; // = NULL
			TmpElement->pNext = NewElement;
			NewElement->pPrev = TmpElement;	
		}	
	}
}

bool PriorityQueue::Remove(int value)
{
	// remove element 
	Element_t *RemoveElement;
	RemoveElement = root->pNext;

	while (RemoveElement != NULL) {
		
		if(RemoveElement->value == value) {
			
			// remove element is the only element in the queue
			if ((RemoveElement->pPrev == NULL) && (RemoveElement->pNext == NULL)) {
				root->pNext = RemoveElement->pNext;
			}
			// remove element is the first element in the queue
			else if ((RemoveElement->pPrev == NULL) && (RemoveElement->pNext != NULL))  {
				RemoveElement->pNext->pPrev = NULL;
				root->pNext = RemoveElement->pNext;
			}
			// remove element is not the first element neither the last element in the queue
			else if((RemoveElement->pPrev != NULL) && (RemoveElement->pNext != NULL)) {
				RemoveElement->pPrev->pNext = RemoveElement->pNext;
				RemoveElement->pNext->pPrev = RemoveElement->pPrev;
			}
			// remove element is the last element in the queue
			else if((RemoveElement->pPrev != NULL) && (RemoveElement->pNext == NULL)) {
				RemoveElement->pPrev->pNext = NULL;
			}

			// delete element and return TRUE
			free(RemoveElement);
			RemoveElement = NULL;
			return true;
		}
		else {
			RemoveElement = RemoveElement->pNext;
		}
	}
    return false;
}

PriorityQueue::Element_t *PriorityQueue::Find(int value)
{
	
   	// find element 
	Element_t *FindElement;
	FindElement = NULL;
	FindElement = root->pNext;

	while (FindElement != NULL) {
		
		if(FindElement->value == value) {
			return FindElement;
		}
		else {
			FindElement = FindElement->pNext;
		}
	}
	
    return NULL;
}

PriorityQueue::Element_t *PriorityQueue::GetHead()
{	
	Element_t *HeadElement;
	HeadElement = root->pNext;
    return HeadElement;
}

/*** Konec souboru tdd_code.cpp ***/
